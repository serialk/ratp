#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

import datetime
import functools
import re
import requests
import unicodedata

URLAPI = 'http://metro.breizh.im/dev/ratp_api.php'

def normalize(s):
    return ''.join(c for c in unicodedata.normalize('NFD', s)
                   if unicodedata.category(c) != 'Mn').lower()


class Api:
    def query(**kwargs):
        return requests.get(URLAPI, params=kwargs).json()

    line_list = functools.partial(query, action='getLineList')
    direction_list = functools.partial(query, action='getDirectionList')
    station_list = functools.partial(query, action='getStationList')
    schedule = functools.partial(query, action='getSchedule')


class Line:
    def __init__(self, json):
        self.id = int(json['id'])
        self.name = json['line']
        self.type_id = int(json['type_id'])
        self.type_name = json['type_name']
        self.dirs_id = None
        self.dirs = None
        self.sts = None

    def retrieve_directions(self):
        r = Api.direction_list(line=self.id)['directions'][0]
        self.dirs_id = int(r['id'])
        self.dirs = r['direction'].split(' - ')[0].split(' / ')

    def retrieve_stations(self):
        r = Api.station_list(direction=self.directions_id)['stations']
        self.sts = {int(i['id']): Station(i, self) for i in r}

    def find_stations(self, name):
        return [s for s in self.stations.values()
                if normalize(name) in normalize(s.name)]

    def find_station(self, name):
        l = self.find_stations(name)
        if len(l) == 0:
            raise AttributeError("Station not found")
        if len(l) > 1:
            l = map(lambda x: x.name, l)
            raise AttributeError('More than one station matching '
                                 '"{}": {}'.format(name, ', '.join(l)))
        return l[0]

    def get_station(self, id_station):
        if not self.sts:
            self.retrieve_stations()
        return self.sts

    @property
    def directions(self):
        if not self.dirs:
            self.retrieve_directions()
        return self.dirs

    @property
    def directions_id(self):
        if not self.dirs_id:
            self.retrieve_directions()
        return self.dirs_id

    @property
    def stations(self):
        if not self.sts:
            self.retrieve_stations()
        return self.sts

    def __repr__(self):
        return '<Line {}>'.format(self.name)


class Station:
    def __init__(self, json, parent):
        self.id = int(json['id'])
        self.name = json['station']
        self.line = parent

    def schedule(self):
        return Schedule(self)

    def __repr__(self):
        return '<Station "{}">'.format(self.name)


# Station status
NORMAL        = 0
UNAVAILABLE   = 1
FINISHED      = 2
PERTURBATIONS = 4
FIRST         = 5
LAST          = 6
LONG          = 7

# Schedule status
WAIT     = 0
HERE     = 1
ARRIVING = 2

station_status = {
    'indispo': UNAVAILABLE,
    'termine': FINISHED,
    'perturb': PERTURBATIONS,
    'premier': FIRST,
    'dernier': LAST,
    'suivant': LONG,
}

schedule_status = {
    'approche': (ARRIVING, 0),
    'arret':    (HERE,     0),
}

class Schedule:
    def __init__(self, station):
        self.station = station
        r = Api.schedule(line=station.line.id,
                         direction=station.line.directions_id,
                         station=station.id)['schedule']
        self.datetime = datetime.datetime.now()
        self.raw = list(map(lambda x: list(x.items())[0], r))

        self.info = []
        self.status = NORMAL

        for a, b in self.raw:
            cont = False
            for ss, flag in station_status.items():
                if normalize(ss) in normalize(a):
                    self.status |= flag
                    cont = True

            if cont:
                continue  # Don't append to the list if it's an infoline

            st = WAIT
            for ss, flag in schedule_status.items():
                if normalize(ss) in normalize(b):
                    st = flag
                    break

            m = re.search(r'[0-9]+', b)
            i = int(m.group(0)) if m else 0

            self.info.append((a, i, st))

    def __repr__(self):
        return '<Schedule at "{}">'.format(self.station.name)


def lines():
    l = Api.line_list()['lines']
    return {i['line']: Line(i) for i in l}
