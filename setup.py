#!/usr/bin/env python3
#-*- encoding: utf-8 -*-

"""
    ratpy
    ~~~~~

    ratpy is an API for the real time schedules provided by the RATP parisian
    transport service

    :copyright: Copyright 2013-2012 Antoine Pietri
    :license: Beerware
"""

from distutils.core import setup

setup(
    name = 'ratpy',
    version = '0.1',
    url = 'http://bitbucket.org/serialk/ratp',
    license = 'Beerware',
    author = 'Antoine Pietri',
    author_email = 'antoine.pietri1@gmail.com',
    description = 'A RATP API.',
    long_description = __doc__,
    keywords = 'ratp api',
    py_modules = ['ratp'],
    platforms = 'any',
)
